class Player {
    constructor(){
        this.posX = width/2;
        this.posY = height-50;
        this.speed = 20;
        this.size = 50;
    }

    moveLeft(){
        this.posX -= this.speed;
    }
    moveRight(){
        this.posX += this.speed;
    }

    show(){
        fill(255);
        stroke(0);
        rect(this.posX, this.posY, this.size);

    }
}

