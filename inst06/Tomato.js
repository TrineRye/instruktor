class Tomato {
    constructor() {
        this.posX = random(0, width);
        this.posY = random(-100, -50);
        this.speed = random(0.2, 2);
    }

    move(){
        this.posY += this.speed;
        if (this.posY > height+50){
            this.posY = random(-100, -50);
        }
    }

    show() {
        fill(255, 0, 0);
        noStroke();
        ellipse(this.posX, this.posY, 50, 40);
        fill(0, 255, 0);
        ellipse(this.posX, this.posY - 25, 5, 20);
    }
}

