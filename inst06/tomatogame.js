let tomato = [];
let player;
let point = 0;

let antal = 15;

function setup() {
    createCanvas(500, 500);
    background(240);
    for (let i = 0; i < antal; i++) {
        tomato.push(new Tomato());
    }

    player = new Player();

}

function draw() {
    background(240);
    spawnTomato();
    player.show();
    checkCollision();
    textAlign(CENTER);
    fill(0);
    text("Point: "+point, width/2, 50)

}

function spawnTomato() {
    for (let i = 0; i < tomato.length; i++) {
        tomato[i].move();
        tomato[i].show();
    }
}

function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        player.moveLeft();
    }
    if (keyCode === RIGHT_ARROW) {
        player.moveRight();
    }
}

function checkCollision() {
    let distance;
    for (let i = 0; i < tomato.length; i++) {

        distance = dist(player.posX + player.size/2, player.posY + player.size/2, tomato[i].posX, tomato[i].posY);

        if (distance < player.size/2) {
            tomato.splice(i, 1);
            point++ // betyder at man lægger 1 til variablen point
            tomato.push(new Tomato());
           
        
        }
    }

}


